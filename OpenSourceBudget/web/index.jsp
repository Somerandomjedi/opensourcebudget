<%-- 
    Document   : index
    Created on : 14/04/2016, 8:23:44 PM
    Author     : Daniel
--%>
<%@page import="opensourcebudget.*;"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<!--This is basically the same file as the one that i made..... Ben -->

<%@page import="opensourcebudget.User"%>
<%@include file="/WEB-INF/jspf/template_defs.jsp" %>
<%@include file="/WEB-INF/jspf/HomePageHead.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
    
    <%-- 
    The If block is used to display the username of the user logged in.
    If there is no user logged in, then it will redirect to the user to login.jsp
    --%>
     <%        
            if( User.getFromSession(request) != null) { %>
                    <h1>Welcome <%= User.getFromSession(request).getUsername() %></h1>
                <% } else { 
                    response.sendRedirect("login.jsp");
                } 
    %>
    <section>
    <%
    ResultSet rs = User.getTransactionsFromDatabase(User.getFromSession(request).getId()); 
    float total = 0;
    int count = 0;
    
    //I want a Calendar to display the current date to the user, and a Date to compare whether things are before/after 
    GregorianCalendar currentCalendar = new GregorianCalendar();
    Date currentDate = new Date();
    out.println("<h2>Current date: " + currentCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US) + " " + 
            currentCalendar.get(Calendar.DATE) + ", " + currentCalendar.get(Calendar.YEAR) + "</h2>");
    
    //Specifying the type of object (a Transaction) causes Netbeans to flip its shit
    //So I don't do that
    ArrayList<Transaction> futureTransactions = new ArrayList();
    ArrayList<Transaction> pastTransactions = new ArrayList();
    
    while (rs.next()) {
        Transaction newTransaction = new Transaction(
            rs.getInt("id"), rs.getInt("userID"), rs.getFloat("quantity"), rs.getDate("date"), rs.getString("description"), 
            rs.getBoolean("isExecuted"), rs.getBoolean("shouldAutoExecute"));
        
     total = total + rs.getFloat("quantity");
     count++;
     
     if (rs.getDate("date").before(currentDate)) {
         pastTransactions.add(newTransaction);
     } else {
         futureTransactions.add(newTransaction);
     }
     
    }
    
    out.println("<h3>Future Transactions:</h3>");
    if (futureTransactions.size() == 0) {
        out.println("<p>None</p>");
    } else {
        out.println("<ul class=\"transactionList\">");
        for (int i = 0; i < futureTransactions.size(); i++) {
            out.println("<li>" + futureTransactions.get(i).getQuantity() + ": " + 
                    futureTransactions.get(i).getDescription() + "</li>");
        }
        out.println("</ul>");
    }
    
    out.println("<h3>Past Transactions:</h3>");
    
    if (pastTransactions.size() == 0) {
        out.println("<p>None</p>");
    } else {
        out.println("<ul class=\"transactionList\">");
        for (int i = 0; i < pastTransactions.size(); i++) {
            out.println("<li>" + pastTransactions.get(i).getQuantity() + ": " + 
                    pastTransactions.get(i).getDescription() + "</li>");
        }
        out.println("</ul>");
    }
    
    out.println("<h2>Current Balance: $" + total + "</h2>");
    %>
    </section>
    <% if (count > 0) { %>
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
    <%}%>   
<%@include file="/WEB-INF/jspf/template_tail.jsp" %>
