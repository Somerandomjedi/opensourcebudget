/*  
    Document   : registration
    Created on : 09/05/2016, 3:49:03 PM
    Author     : Thomas Grant 30113683
 */

function validateUser(){
    
    if (document.querySelector("input[name=Username]").value ===null || 
        document.querySelector("input[name=Username]").value.trim() ===""){
     
        alert("Please fill out username");
        return false;
    }
    if (document.querySelector("input[name=Password]").value ===null || 
        document.querySelector("input[name=Password]").value.trim() ===""){
     
        alert("Please fill out password");
        return false;
    }
    if (document.querySelector("input[name=Password]").value.length <6){
     
        alert("Password need to be at least 6 characters long");
        return false;
    }
    
    // If execution gets here then the form is valid
    return true;
}
