<%-- 
    Document   : HomePageHead
    Created on : Apr 29, 2016, 3:33:00 PM
    Author     : Ben
--%>
<%@page import="opensourcebudget.User"%>
<%-- 
    Document   : template_head
    Created on : Mar 22, 2016, 7:03:32 AM
    Author     : Ben
--%>
<%@page import="opensourcebudget.util.ScriptBuilder"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel="stylesheet" type="text/css" href="css/common.css">

                <% ScriptBuilder sc = new ScriptBuilder();
         String script = sc.build(User.getFromSession(request).getId());
         out.println(script);
        
         //error code change to display something more useful....
         if(script.compareToIgnoreCase("") == 0 ){
             out.println("No data");
         }
        %>
        

    </head>
    <body>
        <img src="Logo png.png" alt="The logo of Banana Budgeting" style="width:896px;height:155px;">
        <header>
            <ul>
                <li><a href="Transaction.jsp">Make Transaction</a></li> 
                <li><a href="AccountManagement.jsp">Account Management</a></li> 
                <li><a href="Logout"> Logout</a></li>  
            </ul>
        </header>   
            <!--place a standard page header in here home -->
    <main>


 
