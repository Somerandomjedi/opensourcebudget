<%-- 
    Document   : Transaction
    Created on : Apr 28, 2016, 2:48:27 PM
    Author     : Ben
--%>

<%@page import="opensourcebudget.User"%>

<link rel="stylesheet" type="text/css" href="css/common.css">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
         <%        
            if( User.getFromSession(request) != null) { %>
                    <h1>Welcome <%= User.getFromSession(request).getUsername() %></h1>
                <% } else { 
                    response.sendRedirect("login.jsp");
                } 
            User.getFromSession(request).getId();
        %>
        <h1>Read code comments for discripton of what is to be on this page</h1>
        <!-- 
        id
        userID
        quantity
        date
        description
        isExecuted
        shouldAutoExecute 
        
            private int id;
            private int userID;
            private float quantity;
            private Date date;
            private String description;
            private boolean isExecuted;
            private boolean shouldAutoExecute;
        -->
        
        <!--Table for the submission of a new transaction--> 
        <section class="half">
            <!-- 
            I don't belive that i require the adduser action or the return validateUser
            I will replace those bits of code with the correct ones once a servlet is built
            --> 
       <form action="NewTransaction" method="post"> 
            <!--
            Transactions need to be able to indicate if it is a withdrawal or if it is a deposit
            If it is a once off or a re-occuring transaction, later when the ability to have multipal accounts
            is added in this will need to be able to indicate what account will have the transaction on it.
            The Transaction listing needs to have a category assigned to the transaction
            -->
            <!--Example of form body -->
            <input type="hidden" name="id" value="<%= User.getFromSession(request).getId() %>"/>
            
          Transaction Reasion: <input type="text" name="Description" value="" required /><br> 
            <br>
          Auto Executed: <input type="checkbox" name="shouldAutoExecute" checked="checked" /> <br> 
            <br>
          Date: <input type="date" name="Date" /><br>
            <br>
          Amount <input type="number" name="Quantity" /> <br>
            <br>
            <input type="submit" value="Save">
        </form>
             <a href="index.jsp"><button>back</button></a>
            
    </section>