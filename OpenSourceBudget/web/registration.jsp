<%-- 
    Document   : registration
    Created on : 02/05/2016, 12:35:45 PM
    Author     : Thomas Grant 30113683
--%>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/template_defs.jsp" %>
<link rel="stylesheet" type="text/css" href="css/common.css">
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script type="text/javascript" src="javascript/registration.js"></script>
        
    <%-- Title --%>
    <h2 style="text-align:center;">Registration</h2>
    <section class="half">
        
        <%-- Form that is passed to AddUser.java --%>
        <form action="AddUser" onsubmit="return validateUser();" method="post">
            Username: <input type="text" name="Username" value="" required><br>
            <br>
            Password: <input type="password" name="Password" value="" required><br>
            <br>
            <input type="submit" value="AddUser">
        </form>
        
        <%-- Link back to login.jsp --%>
        <a href="login.jsp">Go back to Login</a>
        
    </section>
<%@include file="/WEB-INF/jspf/template_tail.jsp" %>

