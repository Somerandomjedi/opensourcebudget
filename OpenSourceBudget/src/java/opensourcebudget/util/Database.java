
package opensourcebudget.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Daniel
 */

//NOTE: This class was adapted from code originally written by Matt Dwyer.
//It's very, very similar because any major changes would essentially only be for the purpose of making it look different.

public class Database {
    
    //Information to access the database
    private static final String DATABASE_NAME = "testbudgetingdatabase";
    private static final String DATABASE_USERNAME = "dpm";
    private static final String DATABASE_PASSWORD = "bananabudget";
    private static final String DATABASE_LOCATION = "testbudgetingdb.cfdmyxn2fbq3.ap-southeast-2.rds.amazonaws.com:3306";
    
    //Using a singleton
    public static Database _instance = new Database();
    
    //Actual connection to the database
    private Connection handle;
    
    //Private constructor because singleton
    //This whole method basically was copy-pasted
    private Database() {
        try {
            // Load the MySQL driver and open a connection
            Class.forName("com.mysql.jdbc.Driver");
            //Database is currently expected to be hosted locally. If we change this to go to Ben's amazon database, this
            //is where we change it!
            this.handle = DriverManager.getConnection(
                "jdbc:mysql://" + DATABASE_LOCATION +"/" + DATABASE_NAME + "?user=" + DATABASE_USERNAME + "&password=" + DATABASE_PASSWORD);
            
        } catch (ClassNotFoundException cnfe) {
            System.out.println("=== MySQL DRIVER DOES NOT EXIST ===");
            cnfe.printStackTrace(System.err);
            System.exit(-1); // Fatal error
            
        } catch (SQLException sqle) {
            System.out.println("=== FAILED TO CONNECT TO DATABASE ===");
            sqle.printStackTrace(System.err);
            System.exit(-1); // Fatal error
        }
    }
    
    //Just a method to get a working PreparedStatement from the connection without having access to the Connection directly
    public PreparedStatement prepare(String sql) throws SQLException {
        return this.handle.prepareStatement(sql);
    }
    
    //Runs a query and returns the result set. Assumed to be a SELECT query but probably could be other things.
    public ResultSet select(String sql) throws SQLException {
        return this.handle.createStatement().executeQuery(sql);
    }
    
}
