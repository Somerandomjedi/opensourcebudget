/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opensourcebudget.util;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import opensourcebudget.User;

/**
 *
 * @author Ben
 */
public class ScriptBuilder {
    
    public ScriptBuilder(){
    
    }
    
    public final String build(int id) throws SQLException{
        
        ResultSet transactionsFromDatabase = User.getTransactionsFromDatabase(id);
        ArrayList<String> transDis = new ArrayList<String>();
        ArrayList<Float> transQuantity = new ArrayList<Float>();
        ArrayList<Byte> transIsExecuted = new ArrayList<Byte>();
        ArrayList<Date> transDate = new ArrayList<Date>();
        
         while (transactionsFromDatabase.next()) {
           
            String dis = transactionsFromDatabase.getString("description");
            Float quant = transactionsFromDatabase.getFloat("quantity");
            Byte exe = transactionsFromDatabase.getByte("isExecuted");
            Date date = transactionsFromDatabase.getDate("date");
            
            transDis.add(dis);
            transQuantity.add(quant);
            transIsExecuted.add(exe);
            transDate.add(date);         
        }
        
         String dataRow = "";
         float sum = 0;
         for(int i = 0; i < transDis.size(); i++){
             sum = sum + transQuantity.get(i);
             dataRow = dataRow + "['" + transDis.get(i) + " '," + sum + "],\n"; 
           }
        
         String Stringbuilder =  "var data = google.visualization.arrayToDataTable([\n" +
                 "['Discription', 'Cash'], \n" +
                 dataRow +
                 "]);\n" +
                 "\n";
         
         
         
         
        String scriptStart = "<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
"            <!-- because i don't know how to use the JASON format this script will be replaced\n" +
"                 with a jsp call which will generate the values on page load, any changes in the data will simply \n" +
"                 trigger a page reload because i also don't know how to use AJAX......\n" +
"            --> \n" +
"            <script type=\"text/javascript\">\n" +
"            google.charts.load('current', {'packages':['corechart']});\n" +
"            google.charts.setOnLoadCallback(drawChart);\n" +
"            function drawChart() {\n";
        String scriptEnd = "var options = {\n" +
"                title: 'Cash Flow',\n" +
"                vAxis: {title: 'Value'},\n" +
"                isStacked: true\n" +
"              };\n" +
"\n" +
"              var chart = new google.visualization.SteppedAreaChart(document.getElementById('chart_div'));\n" +
"\n" +
"              chart.draw(data, options);\n" +
"            }\n" +
"          </script>";
        
        
    return scriptStart + "\n" + Stringbuilder + "\n" + scriptEnd;
    
    }  
}