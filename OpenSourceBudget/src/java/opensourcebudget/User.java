
package opensourcebudget;

import opensourcebudget.util.Database;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Daniel
 */
//Mimicking code written by Matt Dwyer.
public class User {
    
    public static final String SESSION_ATTRIBUTE = "user";
    //User class is very simple. We've got hardly any functionality in scope so we just want an ID, username and password.
    private int id;
    private String username;
    private String password;
    
    //Again, mimicking the structure set out by Matt previously, because it works.
    //Constructor takes all the parameters and sets them below
    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
    
    //A constructor without an id, for if it doesn't exist in the database yet
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    //And one with just the id which will try and retrieve the corresponding user from the database.
    //If nothing by that is exists it throws an SQLException
    public User(int id) throws SQLException {
        
        //Get the details from the database
        ResultSet queryResult = getFromDatabase(id);
        if(queryResult == null)
            throw new SQLException("Failed to get user record from database");

        // Move the pointer to the first row
        if(!queryResult.first())
            throw new SQLException("No records in result set");

        // Set all the fields using the data
        this.id = queryResult.getInt("id");
        this.username = queryResult.getString("username");
        
        //Not getting the password here for the sake of mimicking the other project. If this is an issue, bring it up.
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public int getId() {
        return id;
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }
    
    //Database methods below:
    
    public void insertIntoDatabase() throws SQLException {
        String sql  = "INSERT INTO user (username,password)";
               sql += " VALUES (?, SHA2(?, 512));";
        
        PreparedStatement stmt = Database._instance.prepare(sql);
        stmt.setString(1, this.getUsername());
        stmt.setString(2, this.password);
        
        stmt.executeUpdate();
        
        // Fetch the book's id
        String idSQL = "SELECT * FROM user WHERE id=LAST_INSERT_ID();";
        ResultSet queryResult = Database._instance.select(idSQL);
        queryResult.next();
        this.setId(queryResult.getInt("id"));
    }
    
    //Basically only lets you update your username, not the password. Basically just because that's how it was done
    //previously, so if this should be changed let me know. Or just change it I guess.
    public void updateDatabase() throws SQLException {
        String sql  = "UPDATE user SET username=? WHERE id=?;";
        
        PreparedStatement stmt = Database._instance.prepare(sql);
        stmt.setString(1, this.getUsername());
        stmt.setInt(2, this.getId());
        
        stmt.executeUpdate();
    }
    
    private static ResultSet getFromDatabase(int id) {
        ResultSet record = null;
        
        try {
            // Build the SQL query
            String sql = "SELECT * FROM user WHERE id=?;";
            PreparedStatement stmt = Database._instance.prepare(sql);
            stmt.setInt(1, id);
            
            // Get the data
            record = stmt.executeQuery();
            
        } catch (SQLException sqle) {
            System.out.println("=== ERROR OCCURED GETTING USER WITH ID " + id + " ===");
            sqle.printStackTrace(System.err);
        }
        
        return record;
    }
    public static ResultSet getTransactionsFromDatabase(int id){
        ResultSet record = null;
        
        try{
        // Build the SQL query
            String sql = "SELECT * FROM transaction WHERE userID=?;";
            PreparedStatement stmt = Database._instance.prepare(sql);
            stmt.setInt(1, id);
            
            // Get the data
            record = stmt.executeQuery();
            
        } catch (SQLException sqle) {
            System.out.println("=== ERROR OCCURED GETTING TRANSACTIONS FROM WITH ID " + id + " ===");
            sqle.printStackTrace(System.err);
        }
        return record;
    }
    
    public static User getFromSession(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(User.SESSION_ATTRIBUTE);
    }
}