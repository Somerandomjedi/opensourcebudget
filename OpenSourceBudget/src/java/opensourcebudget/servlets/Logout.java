package opensourcebudget.servlets;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static opensourcebudget.User.SESSION_ATTRIBUTE;

/**
 *
 * @author Thomas Grant 30113683
 */
public class Logout extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Disallow GET requests
        HttpSession session = request.getSession(true);
        session.setAttribute(SESSION_ATTRIBUTE, null);
        response.sendRedirect("login.jsp?status=Logout");
    }
    
    @Override
    public String getServletInfo() {
        return "Logout";
    }
}
