/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opensourcebudget.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import opensourcebudget.User;
import opensourcebudget.util.Database;

/**
 *
 * @author Thomas Grant 30113683
 */

//This servlet is used to log the user in
public class Login extends HttpServlet {
    
    int id;
    String username, password;
    ResultSet searchResults = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Disallow GET requests
        response.getWriter().println("Invalid request");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Get the common fields
        this.username = request.getParameter("Username");
        this.password = request.getParameter("Password");
        
        
        this.Login(request, response);
        
    }

    @Override
    public String getServletInfo() {
        return "Login";
    }
    
    //Method used to login and set the user id as the session
    private void Login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            //Create the sql statement
            //Password encryption is SHA2-512
            String sql = "SELECT * FROM user WHERE username = '" + this.username + "' AND password = SHA2('" + this.password + "','512');" ;
        
            // Run the query
            searchResults = Database._instance.select(sql);
            
            //If a result returns (should only be one)
            //It will set the session to be that of the user
            if(searchResults.next()){
                this.id = searchResults.getInt(1);
                HttpSession session = request.getSession(true);
                User theUser = new User(this.id);
                session.setAttribute(User.SESSION_ATTRIBUTE, theUser);
                
                //Redirects to the home page
                response.sendRedirect("index.jsp?status=Login");
            }
            else{
                
                //If failed to login then it will redirect to the login page
                //Error message appears stating why
                response.sendRedirect("login.jsp?status=LoginFailed");
                response.getWriter().println("User doesn't exist");
                
            }
            
            
            
        } catch (SQLException sqle) {
            System.out.println("=== FAILED TO GET USER ===");
            sqle.printStackTrace();
            
            response.getWriter().println("FAILED TO GET USER: " + sqle.getMessage());
        }
    }

}
