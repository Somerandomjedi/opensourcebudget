/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opensourcebudget.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import opensourcebudget.User;
import opensourcebudget.util.Database;

/**
 *
 * @author Thomas Grant 30113683
 */

public class AddUser extends HttpServlet {
    
    int id;
    String username,password;
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Disallow GET requests
        response.getWriter().println("Invalid request");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // Get the common fields
        this.username = request.getParameter("Username");
        this.password = request.getParameter("Password");
        
        if(this.password != null) // Password field only exists on registration page
            this.addUser(request, response);
        else
           //this.updateUser(request, response);
            System.out.println("Test");

    }

    @Override
    public String getServletInfo() {
        return "AddUser";
    }
    
    private void addUser(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            // Create the User
            User theUser = new User(username,password);
            
            String sql = "SELECT * FROM user WHERE username = '" + this.username + "';";
        
            // Run the query
            ResultSet searchResults = Database._instance.select(sql);
            System.out.println(searchResults);
            response.setContentType("text/html"); 
            PrintWriter out = response.getWriter(); 
            RequestDispatcher rd = null;

            
            if(!searchResults.next()){
                
                theUser.insertIntoDatabase();

                response.sendRedirect("login.jsp?status=AddedUser");
                
            }else{
                out.println("<b>Invalid Login Info.</b><br>"); 
                rd = request.getRequestDispatcher("registration.jsp?status=UserAlreadyExists"); 
                rd.include(request, response);
                
                //System.out.println("User already exist");
                //out.println("<p>User already exist</p>");
                //response.sendRedirect("registration.jsp?status=UserAlreadyExist");
                //out.println("<p>User already exist</p>");
                
            }   
            
        } catch (SQLException sqle) {
            System.out.println("=== FAILED TO INSERT USER ===");
            sqle.printStackTrace();
            
            response.getWriter().println("FAILED TO INSERT USER: " + sqle.getMessage());
        }
    }

    /*
    private void updateUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            // Get the User's session
            User theUser = new User(this.email);
            theUser.setFirstname(this.firstname);
            theUser.setLastname(this.lastname);
            theUser.setPhoneNumber(this.phone_number);
            theUser.setAdmin(this.is_admin != null);
            
            // Update the user record
            theUser.updateDatabase();
            
            //And then update the session data so the profile page gets the current user info
            if(theUser.getEmail().equalsIgnoreCase(User.getFromSession(request).getEmail()))
                request.getSession(true).setAttribute(User.SESSION_ATTRIBUTE, theUser);
            
            // Redirect back to the profile page
            response.sendRedirect("profile.jsp?status=SavedChanges&email=" + theUser.getEmail());
            
        } catch (SQLException sqle) {
            System.out.println("=== FAILED TO UPDATE USER ===");
            sqle.printStackTrace();
            
            response.getWriter().println("FAILED TO UPDATE USER: " + sqle.getMessage());
        }
    }
    */

}
