
package opensourcebudget.servlets;

import opensourcebudget.User;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Daniel
 */
public class TestServlet extends HttpServlet {
    
    //Temporary! Just to make sure the database is working.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        try {
            
            //We're making a new user, just because that's a couple lines less than a transaction.
            
            User newUser = new User("blah", "password");
            
            // Insert the new comment
            newUser.insertIntoDatabase();
            
            // Redirect back to the view book page
            response.sendRedirect("index.jsp");
            
        } catch (SQLException sqle) {
            System.out.println("=== FAILED TO INSERT USER ===");
            sqle.printStackTrace();
            
            response.getWriter().println("FAILED TO INSERT USER: " + sqle.getMessage());
        }
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        // Disallow GET requests
        response.getWriter().println("Invalid request");
    }
    
    @Override
    public String getServletInfo() {
        return "TestServlet";
    }
    
}
