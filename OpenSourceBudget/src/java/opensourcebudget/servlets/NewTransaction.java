
package opensourcebudget.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import opensourcebudget.Transaction;

/**
 *
 * @author Ben
 */
public class NewTransaction extends HttpServlet{
   /* private int id;
            private int userID;
            private float quantity;
            private Date date;
            private String description;
            private boolean isExecuted;
            private boolean shouldAutoExecute;*/
    String description, autoExecute, ids; 
    int userID, id;
    String quantity;
    float ammount;
    Date date;
    boolean isExecuted, shouldAutoExecute;
    
    private Transaction tr;
    
    
    String email, password, firstname, lastname, phone_number, is_admin;
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Disallow GET requests
        response.getWriter().println("Invalid request");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // Get the common fields
        this.quantity = request.getParameter("Quantity");
        this.description = request.getParameter("Description");
        this.autoExecute = request.getParameter("shouldAutoExecute");
        this.ids = request.getParameter("id");
        System.out.println(ids);
        id = Integer.parseInt(ids);
        
        ammount = Float.parseFloat(quantity);
        date = new Date(2016, 12, 3);
        isExecuted = Boolean.parseBoolean(autoExecute);
        
        //Test code to insure things are working soz dan
        tr = new Transaction(1, id, ammount,  date, description, true, isExecuted);
        try {
            tr.insertIntoDatabase();
        } catch (SQLException ex) {
            Logger.getLogger(NewTransaction.class.getName()).log(Level.SEVERE, null, ex);
        }   
        
        //Redirects to the home page
        response.sendRedirect("index.jsp?status=Login");
    }
}
