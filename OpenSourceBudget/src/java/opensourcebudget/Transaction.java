
package opensourcebudget;

import opensourcebudget.util.Database;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Daniel
 */

//Mimicking code written by Matt Dwyer.
public class Transaction {
    
    private int id;
    private int userID;
    private float quantity;
    private Date date;
    private String description;
    private boolean isExecuted;
    private boolean shouldAutoExecute;
    
    //Again, mimicking the structure set out by Matt previously, because it works.
    //Constructor takes all the parameters and sets them below
    public Transaction(int id, int userID, float quantity, Date date, String description, 
            boolean isExecuted, boolean shouldAutoExecute) {
        
        this.id = id;
        this.userID = userID;
        this.quantity = quantity;
        this.date = date;
        this.description = description;
        this.isExecuted = isExecuted;
        this.shouldAutoExecute = shouldAutoExecute;
        
    }
    
    //A constructor without an id, for if it doesn't exist in the database yet
    public Transaction(int userID, float quantity, Date date, String description, 
            boolean isExecuted, boolean shouldAutoExecute) {
        
        this.userID = userID;
        this.quantity = quantity;
        this.date = date;
        this.description = description;
        this.isExecuted = isExecuted;
        this.shouldAutoExecute = shouldAutoExecute;
    }
    
    //And one with just the id which will try and retrieve the corresponding transaction from the database.
    //If nothing by that is exists it throws an SQLException
    public Transaction(int id) throws SQLException {
        //Get the details from the database
        ResultSet queryResult = getFromDatabase(id);
        if(queryResult == null)
            throw new SQLException("Failed to get transaction record from database");

        // Move the pointer to the first row
        if(!queryResult.first())
            throw new SQLException("No records in result set");

        // Set all the fields using the data
        this.id = queryResult.getInt("id");
        this.userID = queryResult.getInt("userID");
        this.quantity = queryResult.getFloat("quantity");
        this.date = queryResult.getDate("date");
        this.description = queryResult.getString("description");
        this.isExecuted = queryResult.getBoolean("isExecuted");
        this.shouldAutoExecute = queryResult.getBoolean("isExecuted");
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setUserID(int userID) {
        this.userID = userID;
    }
    
    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setIsExecuted(boolean isExecuted) {
        this.isExecuted = isExecuted;
    }
    
    public void setShouldAutoExecute(boolean shouldAutoExecute) {
        this.shouldAutoExecute = shouldAutoExecute;
    }
    
    public int getId() {
        return id;
    }
    
    public int getUserID() {
        return userID;
    }
    
    public float getQuantity() {
        return quantity;
    }
    
    public Date getDate() {
        return date;
    }
    
    public String getDescription() {
        return description;
    }
    
    //wonky names sorry
    public boolean getIsExecuted() {
        return isExecuted;
    }
    
    public boolean getShouldAutoExecute() {
        return shouldAutoExecute;
    }
    
    //Database methods below:
    
    public void insertIntoDatabase() throws SQLException {
        String sql  = "INSERT INTO transaction (userID,quantity,date,description,isExecuted,shouldAutoExecute)";
               sql += " VALUES (?,?,?,?,?,?);";
        
        PreparedStatement stmt = Database._instance.prepare(sql);
        stmt.setInt(1, this.getUserID());
        stmt.setFloat(2, this.getQuantity());
        stmt.setDate(3, this.getDate());
        stmt.setString(4, this.getDescription());
        stmt.setBoolean(5, this.getIsExecuted());
        stmt.setBoolean(6, this.getShouldAutoExecute());
        
        stmt.executeUpdate();
        
        // Fetch the transaction's id
        String idSQL = "SELECT * FROM transaction WHERE id=LAST_INSERT_ID();";
        ResultSet queryResult = Database._instance.select(idSQL);
        queryResult.next();
        this.setId(queryResult.getInt("id"));
    }
    
    //Unlike user, lets you change basically everything but the id and userID (for obvious reasons)
    public void updateDatabase() throws SQLException {
        String sql  = "UPDATE transaction SET quantity=?, date=?, description=?, " + 
                "isExecuted=?, shouldAutoExecute=? WHERE id=?;";
        
        PreparedStatement stmt = Database._instance.prepare(sql);
        stmt.setFloat(1, this.getQuantity());
        stmt.setDate(2, this.getDate());
        stmt.setString(3, this.getDescription());
        stmt.setBoolean(4, this.getIsExecuted());
        stmt.setBoolean(5, this.getShouldAutoExecute());
        stmt.setInt(6, this.getId());
        
        stmt.executeUpdate();
    }
    
    private static ResultSet getFromDatabase(int id) {
        ResultSet record = null;
        
        try {
            // Build the SQL query
            String sql = "SELECT * FROM transaction WHERE id=?;";
            PreparedStatement stmt = Database._instance.prepare(sql);
            stmt.setInt(1, id);
            
            // Get the data
            record = stmt.executeQuery();
            
        } catch (SQLException sqle) {
            System.out.println("=== ERROR OCCURED GETTING TRANSACTION WITH ID " + id + " ===");
            sqle.printStackTrace(System.err);
        }
        
        return record;
    }
    
}
